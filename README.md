# BaseImage

## Why doing this?

I started to work with a private cloud hoster. Their system is based on VMware vCloud Director (VCD).
While the system by design and theorie is pretty acceptable, what they actually provide is sometimes ... questionable.
Let us not talk about their outdated hypervisor or several shortcomings of their VCD. Let us talk about the base-images they provide: this is literally a disaster!

A BaseImage is something that I strongly rely on. I __base__ my application and consequentially my success upon it.

When I had to understand that in their Centos Base Image already the package manager was broken, followed by some more annoying aspects around the administration, I decided to go my own way.

Here is my own version of this ... I did it my way!

# Features

## Tools

EPEL is already on board, and that is it.

This is a base-image, it is assumed to be amended. Design your own playbooks to install what you see fit for your application and style of administration.

## Locale

German locale has been activated, but you can have it your way!

# Security

## Services

Unused services like NFS have been disabled. If you need any of these, switch them on and harden them, yourself!

## SSH

SSH has been tightened. Only actual cyphers allowed, user group limited and passwords forbidden. This is a pretty tough setup.

## SSH-keys initially deployed

The playbook will deploy ssh-keys to the base-image. The will be deployed as authorized_keys
for the users vagrant and orgadmin (see below).

There are actually two keys deployed like this:

- the public-key of the user building the image
- a static key from the files-subdirectory. This key is a bit special, it is not registered with GIT. So when you clone the repo, that key-pair does not exist, yet.
The script gen_static_keys will create it. You might decide to copy the public keys from your
JumpServer here.

A word of warning: NEVER copy private keys from anywhere into an image. Others might be able to
access it. And always have your own playbook re-generate your SSH-keys if they are dated as the original keys. You might also centrally manage and audit all existing authorized_keys-files.

# Users

Currently there are two users enabled for SSH: vagrant and orgadmin.

## User vagrant

This is a left-over from the build system. I recommend to remove it.
Whatever you do with it, it a task for your own playbook.

## User orgadmin

I intend to use this user for local system administration and especially for
ansible.

## Administration users

I recommend to deploy administrator-users as individual/personal users on each
involved system. You may do so with a central system, but from my point of view
it is far less hassle to deploy them with a playbook. You will not deal with
passwords on these servers too much. Administrators can hand-in their encrypted
passwords together with their public key for distribution.

This way you can use ansible to audit the user accounts on the related systems,
at least on the Linux-side of the world.

Again: it is recommended to resort to functional users as little as possible.
